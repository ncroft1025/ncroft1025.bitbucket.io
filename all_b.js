var searchData=
[
  ['s0_5finit_0',['S0_INIT',['../namespacetask__control.html#a29b623d7e88f710ea0809abd99745a66',1,'task_control.S0_INIT()'],['../namespacetask___i_m_u.html#a6552518c759f0c58e0bb334e2fbd2318',1,'task_IMU.S0_INIT()'],['../namespacetask__touch.html#ac8bd747f4272339c7888d7fda1eca6f5',1,'task_touch.S0_INIT()'],['../namespacetask__user.html#ad172b5aee322276b27b03b3e3dd99680',1,'task_user.S0_INIT()']]],
  ['s1_5fupdate_1',['S1_UPDATE',['../namespacetask__control.html#a7deafa2797bdceec67c99fad3ac4af60',1,'task_control.S1_UPDATE()'],['../namespacetask___i_m_u.html#aeddf0c4abe744776676ddfc9c6b05a0a',1,'task_IMU.S1_UPDATE()'],['../namespacetask__touch.html#aaf67d04c58bd368e67d04a5ff1a6c615',1,'task_touch.S1_UPDATE()']]],
  ['s1_5fwait_5ffor_5finput_2',['S1_WAIT_FOR_INPUT',['../namespacetask__user.html#a51e0daf7172dfe7df73da74b864c46f1',1,'task_user']]],
  ['set_5fduty_3',['set_duty',['../classmotor__driver_1_1_motor.html#a3363b81b42b951e546faa02ba12566bc',1,'motor_driver::Motor']]],
  ['set_5fkt_4',['set_KT',['../classcontroller__fsfb_1_1_closed_loop.html#ab2aaebb1ffebd8fa3da9a1502f8e05bf',1,'controller_fsfb::ClosedLoop']]],
  ['share_5',['Share',['../classshares_1_1_share.html',1,'shares']]],
  ['shares_6',['shares',['../namespaceshares.html',1,'']]]
];
